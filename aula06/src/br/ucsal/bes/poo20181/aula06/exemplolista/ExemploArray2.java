package br.ucsal.bes.poo20181.aula06.exemplolista;

import java.util.Scanner;

public class ExemploArray2 {

	private static final int QUANTIDADE_NOMES = 5;

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

		String[] nomes = new String[QUANTIDADE_NOMES];

		System.out.println("Informe " + QUANTIDADE_NOMES + " nomes:");
		for (int i = 0; i < nomes.length; i++) {
			nomes[i] = scanner.nextLine();
		}

		// Para excluir o nome da posi��o 2
		for (int i = 2; i < nomes.length - 1; i++) {
			nomes[i] = nomes[i + 1];
		}
		nomes[nomes.length - 1] = null;

		System.out.println("Informe outro nome:");
		nomes[nomes.length - 1] = scanner.nextLine();

		for (int i = 0; i < nomes.length; i++) {
			if (nomes[i] != null) {
				System.out.println("nome" + i + "=" + nomes[i]);
			}
		}

	}

}
