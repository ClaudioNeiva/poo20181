package br.ucsal.bes.poo20181.aula06.exemplolista;

import java.util.ArrayList;
import java.util.Scanner;

public class ExemploList {

	private static final int QUANTIDADE_NOMES = 5;
	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

		// N�O � BEM ASSIM QUE SE FAZ, MAS N�O DISCUTIMOS INTERFACE AINDA, POR
		// ISSO ESTOU FAZENDO NA AULA DE HOJE DESSA FORMA.
		// TAMB�M N�O VOU UTILIZAR GENERICS (TIPAGEM DE LISTA).
		ArrayList nomes = new ArrayList<>();

		System.out.println("Informe " + QUANTIDADE_NOMES + " nomes:");
		for (int i = 0; i < QUANTIDADE_NOMES; i++) {
			nomes.add(scanner.nextLine());
		}

		nomes.remove(2);

		nomes.remove("neiva");
		
		System.out.println("Os nomes informados foram:" + nomes);

		System.out.println("Os nomes informados foram:");
		for (Object nome : nomes) {
			System.out.println(nome);
		}
	}

}
