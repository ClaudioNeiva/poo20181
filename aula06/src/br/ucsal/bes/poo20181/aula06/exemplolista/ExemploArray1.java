package br.ucsal.bes.poo20181.aula06.exemplolista;

import java.util.Scanner;

public class ExemploArray1 {

	private static final int QUANTIDADE_NOMES = 5;

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

		String[] nomes = new String[QUANTIDADE_NOMES];

		System.out.println("Informe " + QUANTIDADE_NOMES + " nomes:");
		for (int i = 0; i < nomes.length; i++) {
			nomes[i] = scanner.nextLine();
		}

		nomes[2] = null;

		System.out.println("Informe outro nome:");
		for (int i = 0; i < nomes.length; i++) {
			if (nomes[i] == null) {
				nomes[i] = scanner.nextLine();
				break;
			}
		}

		for (int i = 0; i < nomes.length; i++) {
			if (nomes[i] != null) {
				System.out.println("nome" + i + "=" + nomes[i]);
			}
		}

	}

}
