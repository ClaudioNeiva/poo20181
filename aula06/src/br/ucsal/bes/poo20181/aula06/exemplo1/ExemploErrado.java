package br.ucsal.bes.poo20181.aula06.exemplo1;

public class ExemploErrado {

	// N�O FA�A ASSIM!!! PELO AMOR DE DEUS! N�O USE ATRIBUTOS DE CLASSE
	// A PARTIR DE INST�NCIAS!
	public static void main(String[] args) {

		Aluno aluno1 = new Aluno();
		Aluno aluno2 = new Aluno();
		Aluno aluno3 = new Aluno();

		// ISSO EST� CONCEITUALMENTE EEEEERRRAAAADDDDOOOO!!!!!
		aluno2.contador = 30;
		aluno1.contador = 10;
		aluno3.contador = 50;

		// ISSO EST� CONCEITUALMENTE EEEEERRRAAAADDDDOOOO!!!!!
		System.out.println("aluno1.contador=" + aluno1.contador);
		System.out.println("aluno2.contador=" + aluno2.contador);
		System.out.println("aluno3.contador=" + aluno3.contador);

	}

}
