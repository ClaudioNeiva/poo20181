package br.ucsal.bes.poo20181.aula06.exemplo2;

import java.util.Date;

public class Aluno {

	private static Integer contador = 0;

	private Integer matricula;

	private String nome;

	private String email;

	private Date dataNascimento;

	public Aluno() {
		contador++;
		matricula = contador;
	}

	public static void setContador(Integer contador){
		Aluno.contador = contador;
	}
	
	public static Integer getContador(){
		return contador;
	}
	
	public Integer getMatricula() {
		return matricula;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

}
