package br.ucsal.bes.poo20181.aula06.exemplo2;

public class ExemploErrado {

	// N�O FA�A ASSIM!!! PELO AMOR DE DEUS! N�O USE M�TODOS DE CLASSE
	// A PARTIR DE INST�NCIAS!
	public static void main(String[] args) {

		Aluno aluno1 = new Aluno();
		Aluno aluno2 = new Aluno();
		Aluno aluno3 = new Aluno();

		// ISSO EST� CONCEITUALMENTE EEEEERRRAAAADDDDOOOO!!!!!
		aluno1.setContador(30);
		aluno2.setContador(10);
		aluno3.setContador(50);

		// ISSO EST� CONCEITUALMENTE EEEEERRRAAAADDDDOOOO!!!!!
		System.out.println("aluno1.contador=" + aluno1.getContador());
		System.out.println("aluno2.contador=" + aluno2.getContador());
		System.out.println("aluno3.contador=" + aluno3.getContador());

	}

}
