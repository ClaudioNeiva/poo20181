package br.ucsal.bes.poo20181.aula11;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListaExemplo2 {

	public static void main(String[] args) {
		// Criar uma lista
		List<String> nomes = new ArrayList<>();

		// Adicionar elementos a uma lista
		nomes.add("claudio");
		nomes.add("antonio");
		nomes.add("maria");
		nomes.add("pedro");
		nomes.add("clara");
		
		// Exibir o conte�do da lista antes da orden��o
		System.out.println("Nomes (como foram informados):");
		for(String nome : nomes){
			System.out.println(nome);
		}
		
		// Ordenar a lista (ordem "alfab�tica" crescente)
		Collections.sort(nomes);
		
		// Exibir o conte�do da lista ap�s da orden��o
		System.out.println("Nomes (em ordem 'alfab�tica' crescente):");
		for(String nome : nomes){
			System.out.println(nome);
		}
		
		// Inverter a ordem dos elementos de uma lista (n�o quer dizer, ordenar de forma decrescente)
		// Como a lista est� ordenada em ordem crescente, inverter a mesma significa ordenar em ordem decrescente.
		Collections.reverse(nomes);
		
		// Exibir o conte�do da lista ap�s a invers�o da ordem
		System.out.println("Nomes (em ordem 'alfab�tica' decrescente):");
		for(String nome : nomes){
			System.out.println(nome);
		}
		
	}
}
