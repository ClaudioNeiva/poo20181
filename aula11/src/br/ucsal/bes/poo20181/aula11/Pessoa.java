package br.ucsal.bes.poo20181.aula11;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Pessoa implements Comparable<Pessoa> {

	private String nome;

	private Double salario;

	private Date dataNascimento;

	@Override
	public int compareTo(Pessoa outraPessoa) {
		// if(salario > outraPessoa.salario){
		// return 1;
		// }
		// if(salario < outraPessoa.salario){
		// return -1;
		// }
		// return 0;
		return salario.compareTo(outraPessoa.salario);
	}

	public Pessoa(String nome, Double salario, Date dataNascimento) {
		super();
		this.nome = nome;
		this.salario = salario;
		this.dataNascimento = dataNascimento;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Double getSalario() {
		return salario;
	}

	public void setSalario(Double salario) {
		this.salario = salario;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	@Override
	public String toString() {
		DateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");
		return "Pessoa [nome=" + nome + ", salario=" + salario + ", dataNascimento=" + sdf.format(dataNascimento) + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataNascimento == null) ? 0 : dataNascimento.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((salario == null) ? 0 : salario.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pessoa other = (Pessoa) obj;
		if (dataNascimento == null) {
			if (other.dataNascimento != null)
				return false;
		} else if (!dataNascimento.equals(other.dataNascimento))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (salario == null) {
			if (other.salario != null)
				return false;
		} else if (!salario.equals(other.salario))
			return false;
		return true;
	}

}
