package br.ucsal.bes.poo20181.aula11;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListaExemplo5 {

	public static void main(String[] args) {
		// Criar uma lista
		List<Double> valores = new ArrayList<>();

		// Adicionar elementos a uma lista
		valores.add(12.34);
		valores.add(2311.33);
		valores.add(23.44);
		valores.add(344d);
		valores.add(45d);

		// Ordenar a lista
		Collections.sort(valores);
	}
}
