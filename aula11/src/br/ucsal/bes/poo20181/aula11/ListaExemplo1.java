package br.ucsal.bes.poo20181.aula11;

import java.util.ArrayList;
import java.util.List;

public class ListaExemplo1 {

	public static void main(String[] args) {
		// Criar uma lista
		List<String> nomes = new ArrayList<>();

		// Adicionar elementos a uma lista
		nomes.add("antonio");
		nomes.add("claudio");
		nomes.add("maria");
		nomes.add("clara");
		// Remover elementos de uma lista
		Boolean removeu = nomes.remove("claudio");
		if (removeu) {
			System.out.println("claudio foi removido da lista");
		} else {
			System.out.println("claudio n�o foi encontrado e portanto n�o pode ser removido");
		}
		// Recuperar elementos em uma lista
		System.out.println("Primeiro elemento da lista=" + nomes.get(0));
		// Pesquisar se um elemento est� na lista
		System.out.println("A lista contem o nome maria? " + nomes.contains("maria"));
	}

}
