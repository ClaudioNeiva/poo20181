package br.ucsal.bes.poo20181.aula11;

import java.util.ArrayList;
import java.util.List;

public class ListaExemplo3 {

	public static void main(String[] args) {
		// Criar uma lista
		List<String> nomes = new ArrayList<>();

		// Adicionar elementos a uma lista
		nomes.add("claudio");
		nomes.add("antonio");
		nomes.add("maria");
		nomes.add("pedro");
		nomes.add("clara");

		// Exibir o conte�do da lista antes da orden��o
		System.out.println(nomes);
	}
}
