package br.ucsal.bes.poo20181.aula11;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class ListaExemplo4 {

	public static void main(String[] args) {
		// Criar uma lista
		List<Pessoa> pessoas = new ArrayList<>();

		// Adicionar pessoas � lista
		Pessoa pessoa1 = new Pessoa("claudio", 50800d, new Date());
		pessoas.add(pessoa1);
		pessoas.add(new Pessoa("maria", 10000d, new Date()));
		pessoas.add(new Pessoa("pedro", 2000d, new Date()));
		pessoas.add(new Pessoa("antonio", 20000d, new Date()));
		pessoas.add(new Pessoa("joaquim", 900d, new Date()));

		// Exibir os objetos na ordem que foram informadas
		System.out.println("Pessoas, na ordem que foi informado:");
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa);
		}

		// Ordenar a lista de pessoas (pelo crit�rio de ordena��o definido no
		// m�todo compareTo da classe Pessoa.
		Collections.sort(pessoas);

		// Exibir os objetos na ordem crescente de sal�rio (como ficou definido
		// no m�todo compareTo da classe Pessoa).
		System.out.println("Pessoas, na ordem crescente de sal�rio:");
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa);
		}

	}

}
