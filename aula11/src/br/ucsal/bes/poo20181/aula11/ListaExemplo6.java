package br.ucsal.bes.poo20181.aula11;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ListaExemplo6 {

	public static void main(String[] args) throws ParseException {

		DateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");

		Date dataNascimento = sdf.parse("06/08/2001");

		Pessoa pessoa1 = new Pessoa("claudio", 100000d, dataNascimento);
		Pessoa pessoa2 = new Pessoa("claudio", 100000d, dataNascimento);

		if (pessoa1 == pessoa2) {
			System.out.println("S�o a mesma inst�ncia. As vari�veis apontam para a mesma inst�ncia.");
		} else {
			System.out.println("N�O s�o a mesma inst�ncia. As vari�veis N�O apontam para a mesma inst�ncia.");
		}

		if (pessoa1.equals(pessoa2)) {
			System.out.println("S�o a mesma pessoa. As vari�veis apontam para a mesma pessoa.");
		} else {
			System.out.println("N�O s�o a mesma pessoa As vari�veis N�O apontam para a mesma pessoa.");
		}

		System.out.println(pessoa1);
	}
}
