package br.ucsal.bes20181.aula15.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import br.ucsal.bes20181.aula15.exceptions.AplicacaoException;

public class Aula15Properties {

	private static final String CONFIG_PROPERTIES_FILENAME = "config.properties";
	private static Properties properties = new Properties();

	static {
		try {
			loadProperties();
		} catch (AplicacaoException e) {
			System.out.println(e.getMessage());
		}
	}

	public static String getAlunoDAOClassName() {
		return properties.getProperty("persistence.alunodao.class");
	}

	private static void loadProperties() throws AplicacaoException {
		InputStream inputStream = null;
		try {
			inputStream = Aula15Properties.class.getClassLoader().getResourceAsStream(CONFIG_PROPERTIES_FILENAME);
			properties.load(inputStream);
		} catch (IOException ex) {
			throw new AplicacaoException("Erro na leitura do arquivo de configurações da aplicação.");
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					throw new AplicacaoException("Erro no fechamento do arquivo de configurações da aplicação.");
				}
			}
		}
	}

}
