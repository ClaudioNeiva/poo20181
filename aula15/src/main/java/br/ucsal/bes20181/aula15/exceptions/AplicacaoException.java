package br.ucsal.bes20181.aula15.exceptions;

public class AplicacaoException extends Exception {

	private static final long serialVersionUID = 1L;

	public AplicacaoException(String message) {
		super(message);
	}
}
