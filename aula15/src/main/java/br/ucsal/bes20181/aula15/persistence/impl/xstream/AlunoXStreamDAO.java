package br.ucsal.bes20181.aula15.persistence.impl.xstream;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.XStream;

import br.ucsal.bes20181.aula15.domain.Aluno;
import br.ucsal.bes20181.aula15.exceptions.AplicacaoException;
import br.ucsal.bes20181.aula15.persistence.AlunoDAO;

public class AlunoXStreamDAO implements AlunoDAO {

	private static final String ALUNOS_FILE = "c:\\trabalho\\alunos.xml";

	private static List<Aluno> alunos = new ArrayList<>();

	private static final XStream xStream = new XStream();

	static {
		try {
			carregarAlunos();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void insert(Aluno aluno) throws AplicacaoException {
		alunos.add(aluno);
		try {
			salvarAlunos();
		} catch (IOException e) {
			throw new AplicacaoException("Erro ao salvar os dados do aluno.");
		}
	}

	public List<Aluno> findAll() {
		return alunos;
	}

	@SuppressWarnings("unchecked")
	private static void carregarAlunos() throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(ALUNOS_FILE));
		try {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append(System.lineSeparator());
				line = br.readLine();
			}

			String conteudoArquivo = sb.toString();

			alunos = (List<Aluno>) xStream.fromXML(conteudoArquivo);

		} finally {
			br.close();
		}
	}

	private static void salvarAlunos() throws AplicacaoException, IOException {
		String alunosString = xStream.toXML(alunos);
		BufferedWriter writer;
		writer = new BufferedWriter(new FileWriter(ALUNOS_FILE));
		writer.write(alunosString);
		writer.close();
	}

}
