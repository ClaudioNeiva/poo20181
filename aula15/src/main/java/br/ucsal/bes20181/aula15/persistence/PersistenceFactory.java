package br.ucsal.bes20181.aula15.persistence;

import br.ucsal.bes20181.aula15.exceptions.AplicacaoException;
import br.ucsal.bes20181.aula15.utils.Aula15Properties;

public class PersistenceFactory {

	private static AlunoDAO alunoDAO = null;

	public static AlunoDAO getAlunoDAO() {
		if (alunoDAO == null) {
			try {
				alunoDAO = createAlunoDAO();
			} catch (AplicacaoException | InstantiationException | IllegalAccessException | ClassNotFoundException e) {
				System.out.println("Erro grave! N�o ser� poss�vel inicializar o sistema:\n" + e.getMessage());
				System.exit(0);
			}
		}
		return alunoDAO;
	}

	private static AlunoDAO createAlunoDAO()
			throws AplicacaoException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		return (AlunoDAO) Class.forName(Aula15Properties.getAlunoDAOClassName()).newInstance();
	}

}
