package br.ucsal.bes20181.aula15.tui;

import java.util.List;
import java.util.Scanner;

import br.ucsal.bes20181.aula15.business.AlunoBC;
import br.ucsal.bes20181.aula15.domain.Aluno;
import br.ucsal.bes20181.aula15.exceptions.AplicacaoException;
import br.ucsal.bes20181.aula15.exceptions.NegocioException;

public class CadastroAlunoTui {

	private static Scanner scanner = new Scanner(System.in);

	public static void incluir() {
		System.out.println("Informe a matricula:");
		Integer matricula = scanner.nextInt();
		scanner.nextLine();
		System.out.println("Informe o nome:");
		String nome = scanner.nextLine();
		try {
			AlunoBC.incluir(matricula, nome);
			System.out.println("Aluno inclu�do com sucesso!");
		} catch (NegocioException | AplicacaoException e) {
			System.err.println(e.getMessage());
		}
	}

	public static void listar() {
		List<Aluno> alunos = AlunoBC.findAll();
		if (alunos.isEmpty()) {
			System.out.println("Nenhum aluno cadastrado.");
		} else {
			System.out.println("Alunos cadastrados:");
			for (Aluno aluno : alunos) {
				System.out.println(aluno);
			}
		}
	}

}
