package br.ucsal.bes20181.aula15.persistence.impl.arraylist;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20181.aula15.domain.Aluno;
import br.ucsal.bes20181.aula15.persistence.AlunoDAO;

public class AlunoArrayListDAO implements AlunoDAO {

	private static List<Aluno> alunos = new ArrayList<>();

	public void insert(Aluno aluno) {
		alunos.add(aluno);
	}

	public List<Aluno> findAll() {
		return alunos;
	}

}
