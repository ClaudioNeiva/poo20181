package br.ucsal.bes20181.aula15.persistence.impl.serialize;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20181.aula15.domain.Aluno;
import br.ucsal.bes20181.aula15.exceptions.AplicacaoException;
import br.ucsal.bes20181.aula15.persistence.AlunoDAO;

public class AlunoSerializeDAO implements AlunoDAO {

	private static final String ALUNOS_FILENAME = "c:\\trabalho\\alunos.dat";

	private static final File ALUNOS_FILE = new File(ALUNOS_FILENAME);

	private static List<Aluno> alunos = new ArrayList<>();

	static {
		try {
			carregarAlunos();
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
	}

	public void insert(Aluno aluno) throws AplicacaoException {
		alunos.add(aluno);
		try {
			salvarAlunos();
		} catch (IOException e) {
			throw new AplicacaoException("Erro ao salvar os dados do aluno.");
		}
	}

	public List<Aluno> findAll() {
		return alunos;
	}

	@SuppressWarnings("unchecked")
	private static void carregarAlunos() throws IOException, ClassNotFoundException {
		ObjectInputStream objInput = new ObjectInputStream(new FileInputStream(ALUNOS_FILE));
		alunos = (ArrayList<Aluno>) objInput.readObject();
		objInput.close();
	}

	private static void salvarAlunos() throws AplicacaoException, FileNotFoundException, IOException {
		ObjectOutputStream objOutput = new ObjectOutputStream(new FileOutputStream(ALUNOS_FILE));
		objOutput.writeObject(alunos);
		objOutput.close();
	}
}
