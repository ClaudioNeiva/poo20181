package br.ucsal.bes20181.aula15.persistence;

import java.util.List;

import br.ucsal.bes20181.aula15.domain.Aluno;
import br.ucsal.bes20181.aula15.exceptions.AplicacaoException;

public interface AlunoDAO {

	void insert(Aluno aluno) throws AplicacaoException;

	List<Aluno> findAll();

}
