package br.ucsal.bes20181.aula15.business;

import java.util.List;

import br.ucsal.bes20181.aula15.domain.Aluno;
import br.ucsal.bes20181.aula15.exceptions.AplicacaoException;
import br.ucsal.bes20181.aula15.exceptions.NegocioException;
import br.ucsal.bes20181.aula15.persistence.AlunoDAO;
import br.ucsal.bes20181.aula15.persistence.PersistenceFactory;

public class AlunoBC {

	private static AlunoDAO alunoDAO = PersistenceFactory.getAlunoDAO();

	public static void incluir(Integer matricula, String nome) throws AplicacaoException, NegocioException {
		Aluno aluno = new Aluno(matricula, nome);
		validar(aluno);
		alunoDAO.insert(aluno);
	}

	public static List<Aluno> findAll() {
		return alunoDAO.findAll();
	}

	private static void validar(Aluno aluno) throws NegocioException {
		if (aluno.getNome() == null || aluno.getNome().isEmpty()) {
			throw new NegocioException("O campo nome � de preenchimento obrigat�rio.");
		}
		if (aluno.getMatricula() == null) {
			throw new NegocioException("O campo matr�cula � de preenchimento obrigat�rio.");
		}
	}

}
