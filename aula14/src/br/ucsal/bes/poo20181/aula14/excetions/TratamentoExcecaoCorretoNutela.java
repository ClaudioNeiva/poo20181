package br.ucsal.bes.poo20181.aula14.excetions;

import java.util.Scanner;

public class TratamentoExcecaoCorretoNutela {

	private static final Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		while (true) {
			int n = obterNumero();
			Long fatorial = null;
			try {
				fatorial = calcularFatorial(n);
				exibirFatorial(n, fatorial);
			} catch (Exception e) {
				// Escrever um arquivo de log com o e.printStackTrace();
				System.out.println("Erro:" + e.getMessage());
			}
		}
	}

	private static int obterNumero() {
		System.out.println("Informe um n�mero:");
		return scanner.nextInt();
	}

	private static long calcularFatorial(Integer n) throws Exception {
		if (n < 0) {
			throw new Exception("N�mero n�o v�lido para c�lculo do fatorial (n=" + n + ")");
		}
		long fat = 1L;
		for (int i = 1; i <= n; i++) {
			fat *= i;
		}
		return fat;
	}

	private static void exibirFatorial(Integer n, Long fatorial) {
		System.out.println("fatorial(" + n + ")=" + fatorial);
	}
}
