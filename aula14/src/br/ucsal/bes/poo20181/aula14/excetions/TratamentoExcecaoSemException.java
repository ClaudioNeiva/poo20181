package br.ucsal.bes.poo20181.aula14.excetions;

import java.util.Scanner;

// NESTA CLASSE SER� FEITO DA FORMA ERRADA!!!! 
// � COMO N�O DEVEMOS FAZER!!!

public class TratamentoExcecaoSemException {

	private static final Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

		int n = obterNumero();

		RetornoFatorial retornoFatorial = calcularFatorial(n);

		if (retornoFatorial.sucesso) {
			exibirFatorial(n, retornoFatorial.fatorial);
		} else {
			System.out.println("Erro ao calculado fatorial. O valor de n n�o � v�lido.");
		}

	}

	private static int obterNumero() {
		System.out.println("Informe um n�mero:");
		return scanner.nextInt();
	}

	// Sem o conhecimento necess�rio,temos uma situ��o onde este m�todo deve ter
	// dois retornos: se teve sucesso ou falha e o valor do fatorial calculado
	// no caso de sucesso.
	// A� come�am as GAMBIARRAS!
	// Duas poss�veis GAMBIS: i) dizer que retornos de valores negativos indicam
	// erro; ii) Seria retornar uma inst�ncia de uma classe que tem dois
	// atributos: situa��o de sucesso e valor do fatorial.
	private static RetornoFatorial calcularFatorial(int n) {
		// O m�todo calcularFatorial, deveria indicar que n�meros menores que
		// zero n�o podem ter seu fatorial calculado. Ou seja, temos duas
		// situa��es poss�veis neste m�todo: a) foi passado um n�mero maior ou
		// igual a zero e o fatorial deve ser calculado; b) foi passado um
		// n�mero menor que zero e deve ser retornado um indicativo de erro.
		if (n < 0) {
			return new RetornoFatorial(false, null);
		}
		long fat = 1L;
		for (int i = 1; i <= n; i++) {
			fat *= i;
		}
		return new RetornoFatorial(true, fat);
	}

	private static void exibirFatorial(int n, long fatorial) {
		System.out.println("fatorial(" + n + ")=" + fatorial);
	}

}
