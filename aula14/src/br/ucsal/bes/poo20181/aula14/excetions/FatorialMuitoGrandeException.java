package br.ucsal.bes.poo20181.aula14.excetions;

public class FatorialMuitoGrandeException extends Exception {

	private static final long serialVersionUID = 1L;

	public FatorialMuitoGrandeException(Integer valor) {
		super("O valor do fatorial para o n�mero informado (" + valor
				+ ") extrapola a capacidade do m�todo de c�lculo.");
	}

}
