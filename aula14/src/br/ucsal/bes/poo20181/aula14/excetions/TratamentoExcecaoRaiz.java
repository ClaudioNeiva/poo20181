package br.ucsal.bes.poo20181.aula14.excetions;

import java.util.Scanner;

public class TratamentoExcecaoRaiz {

	private static final Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		while (true) {
			int n = obterNumero();
			Long fatorial = null;
			try {
				fatorial = calcularFatorial(n);
				exibirFatorial(n, fatorial);
			} catch (ValorInvalidoException e) {
				// Escrever um arquivo de log com o e.printStackTrace();
				System.out.println("Erro:" + e.getMessage());
			} catch (FatorialMuitoGrandeException e) {
				// Escrever um arquivo de log com o e.printStackTrace();
				System.out.println("DOIDO!!!!!!" + e.getMessage());
			}
		}
	}

	private static int obterNumero() {
		System.out.println("Informe um n�mero:");
		return scanner.nextInt();
	}

	private static long calcularFatorial(Integer n) throws ValorInvalidoException, FatorialMuitoGrandeException {
		if (n < 0) {
			throw new ValorInvalidoException(n);
		}
		if (n > 12) {
			throw new FatorialMuitoGrandeException(n);
		}
		long fat = 1L;
		for (int i = 1; i <= n; i++) {
			fat *= i;
		}
		return fat;
	}

	private static void exibirFatorial(Integer n, Long fatorial) {
		System.out.println("fatorial(" + n + ")=" + fatorial);
	}
}
