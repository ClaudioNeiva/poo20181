package br.ucsal.bes.poo20181.aula14.excetions;

public class ValorInvalidoException extends Exception {

	private static final long serialVersionUID = 1L;

	public ValorInvalidoException(Integer valor) {
		super("N�mero n�o v�lido (" + valor + ")");
	}

}
