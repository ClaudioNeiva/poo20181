package br.ucsal.bes.poo20181.aula14.ordenacao;

import java.util.Comparator;

// ISSO N�O � USUAL. GERALMENTE N�O � CRIADA UMA CLASSE NOMEADA PARA O TRATAMENTO DE COMPARADORES.
// N�O FA�A ISSO!
public class ComparadorPessoaAnoNascimento implements Comparator<Pessoa> {

	@Override
	public int compare(Pessoa o1, Pessoa o2) {
		return o1.getAnoNascimento().compareTo(o2.getAnoNascimento());
	}

}
