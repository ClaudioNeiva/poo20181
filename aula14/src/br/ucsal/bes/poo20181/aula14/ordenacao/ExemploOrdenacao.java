package br.ucsal.bes.poo20181.aula14.ordenacao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ExemploOrdenacao {

	public static void main(String[] args) {
		List<Pessoa> pessoas = new ArrayList<>();

		pessoas.add(new Pessoa("Claudio", 1975, "Joana"));
		pessoas.add(new Pessoa("Antonio", 1973, "Claudia"));
		pessoas.add(new Pessoa("Maria", 1975, "Joana"));
		pessoas.add(new Pessoa("Clara", 1977, "Joana"));
		pessoas.add(new Pessoa("Clarissa", 2005, "Mariana"));
		pessoas.add(new Pessoa("Joaquim", 2005, "Claudia"));

		// Exibir a lista de pessoas em ordem crescente de nome da pessoa.
		Collections.sort(pessoas);
		System.out.println("Pessoas, por ordem crescente de nome:");
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa);
		}

		// Exibir a lista de pessoas em ordem crescente de ano de nascimento.

		// N�O fa�a isso! N�o � usual. N�o � como o pessoal de Java normalmente
		// trabalha!
		// ComparadorPessoaAnoNascimento comparadorPessoasAnoNascimento = new
		// ComparadorPessoaAnoNascimento();
		// Collections.sort(pessoas, comparadorPessoasAnoNascimento);

		Collections.sort(pessoas, new Comparator<Pessoa>() {
			@Override
			public int compare(Pessoa o1, Pessoa o2) {
				return o1.getAnoNascimento().compareTo(o2.getAnoNascimento());
			}
		});
		System.out.println("\nPessoas, por ordem crescente de ano de nascimento:");
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa);
		}

		// Ordena��o multin�vel, primeiro por nome da m�e e depois por nome da
		// pessoa.
		Collections.sort(pessoas, new Comparator<Pessoa>() {
			@Override
			public int compare(Pessoa o1, Pessoa o2) {
				int resultado = o1.getNomeMae().compareTo(o2.getNomeMae());
				if (resultado == 0) {
					resultado = o1.getNome().compareTo(o2.getNome());
				}
				return resultado;
			}
		});
		System.out.println(
				"\nPessoas, por ordem crescente de nome da m�e e, para a mesma m�e, em ordem crescente de nome da pessoa:");
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa);
		}

	}

}
