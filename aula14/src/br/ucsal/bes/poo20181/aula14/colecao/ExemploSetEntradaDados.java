package br.ucsal.bes.poo20181.aula14.colecao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class ExemploSetEntradaDados {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		obterNomesDistintos();
	}

	// Solicitar que o usu�rio informe 4 nomes distintos. Quando um nome
	// repetido for informado, ele deve ser ignorado.
	private static void obterNomesDistintos() {
		Set<String> nomes = new HashSet<>();
		do {
			System.out.println("Informe um nome (usando Set):");
			String nome = scanner.nextLine();
			nomes.add(nome);
		} while (nomes.size() < 4);
		System.out.println("Nomes distintos informados:" + nomes);
	}

	// N�O devemos fazer assim!!!!
	private static void obterNomesDistintosUsandoList() {
		List<String> nomes = new ArrayList<>();
		do {
			System.out.println("Informe um nome (usando List):");
			String nome = scanner.nextLine();
			if (!nomes.contains(nome)) {
				nomes.add(nome);
			}
		} while (nomes.size() < 4);
		System.out.println("Nomes distintos informados:" + nomes);
	}
}
