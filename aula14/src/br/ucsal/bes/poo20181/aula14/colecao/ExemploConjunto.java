package br.ucsal.bes.poo20181.aula14.colecao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class ExemploConjunto {

	public static void main(String[] args) {
		exemploList();
		System.out.println("*******************");
		exemploSetHashSet();
		System.out.println("*******************");
		exemploSetLinkedHashSet();
		System.out.println("*******************");
		exemploSetTreeSet();
	}

	private static void exemploSetHashSet() {
		// O Set n�o permite a duplicidade de valores.
		// � utilizado o m�todo equals para definir quando um objeto h� est� no
		// conjunto. Se voc� estiver utilizando o HashSet (o m�todo hashCode �
		// utilizado para definir essa duplicidade).
		// A depender da implementa��o o conjuntos n�o preserva a ordem
		// (HashSet). Mas existem outras implementa��es que preservam a ordem
		// (LinkedHashSet) e que at� mesmo ordenam os itens do conjunto
		// (TreeSet).
		Set<String> nomes = new HashSet<>();
		nomes.add("Antonio");
		nomes.add("Pedreira");
		nomes.add("Claudio");
		nomes.add("Maria");
		nomes.add("Pedreira");
		nomes.add("Maria");
		nomes.add("Pedreira");

		System.out.println("A interface Set, utilizando a implementa��o HashSet:");
		System.out.println(
				"N�o � poss�vel recuperar itens por posi��o: a interface set n�o possui m�todo get, ou seja, n�o � poss�vel fazer nomes.get(2)");
		System.out.println("Os nomes informados foram (observe que n�o existem valores duplicados): ");
		for (String nome : nomes) {
			System.out.println(nome);
		}
	}

	private static void exemploSetLinkedHashSet() {
		// Utilizando a implementa��o LinkedHashSet, a ordem dos elementos ser�
		// preservada.
		Set<String> nomes = new LinkedHashSet<>();
		nomes.add("Antonio");
		nomes.add("Pedreira");
		nomes.add("Claudio");
		nomes.add("Maria");
		nomes.add("Pedreira");
		nomes.add("Maria");
		nomes.add("Pedreira");

		System.out.println("A interface Set, utilizando a implementa��o LinkedHashSet:");
		System.out.println(
				"N�o � poss�vel recuperar itens por posi��o: a interface set n�o possui m�todo get, ou seja, n�o � poss�vel fazer nomes.get(2)");
		System.out.println("Os nomes informados foram (observe que n�o existem valores duplicados): ");
		for (String nome : nomes) {
			System.out.println(nome);
		}
	}

	private static void exemploSetTreeSet() {
		// Utilizando a implementa��o TreeSet, os elementos ser�o ordenados.
		Set<String> nomes = new TreeSet<>();
		nomes.add("Antonio");
		nomes.add("Pedreira");
		nomes.add("Claudio");
		nomes.add("Maria");
		nomes.add("Pedreira");
		nomes.add("Maria");
		nomes.add("Pedreira");

		System.out.println("A interface Set, utilizando a implementa��o TreeSet:");
		System.out.println(
				"N�o � poss�vel recuperar itens por posi��o: a interface set n�o possui m�todo get, ou seja, n�o � poss�vel fazer nomes.get(2)");
		System.out.println("Os nomes informados foram (observe que n�o existem valores duplicados): ");
		for (String nome : nomes) {
			System.out.println(nome);
		}
	}

	private static void exemploList() {
		// Permite o armazenamento de ocorr�ncias duplicadas.
		// Perserva a ordem dos valores informados.
		// Permite a recupera��o de itens por posi��o
		List<String> nomes = new ArrayList<>();
		nomes.add("Antonio");
		nomes.add("Pedreira");
		nomes.add("Claudio");
		nomes.add("Maria");
		nomes.add("Pedreira");
		nomes.add("Maria");
		nomes.add("Pedreira");

		System.out.println("LIST:");
		System.out.println("O terceiro nome informado foi: " + nomes.get(2));
		System.out.println("Os nomes informados foram (observe os valores duplicados): ");
		for (String nome : nomes) {
			System.out.println(nome);
		}
	}

}
