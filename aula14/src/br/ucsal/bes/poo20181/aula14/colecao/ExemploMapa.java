package br.ucsal.bes.poo20181.aula14.colecao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExemploMapa {

	public static void main(String[] args) {
		exemploMapaSimples();
		exmeploMapaComListas();
	}

	private static void exmeploMapaComListas() {
		// Map <nome-do-estado, lista-cidades-daquele-estado>
		Map<String, List<String>> mapa = new HashMap<>();

		List<String> cidadesBahia = Arrays
				.asList(new String[] { "Salvador", "Feira de Santana", "Cajazeiras X", "Alagoinhas" });
		mapa.put("Bahia", cidadesBahia);

		List<String> cidadesSaoPaulo = new ArrayList<>();
		cidadesSaoPaulo.add("Cubat�o");
		cidadesSaoPaulo.add("Campos do Jord�o");
		cidadesSaoPaulo.add("Cajazeiras XI");
		mapa.put("S�o Paulo", cidadesSaoPaulo);

		// Adicionar uma cidade depois que colocamos a lista no mapa.
		mapa.get("S�o Paulo").add("Cajazeiras XXX");

		System.out.println("Cidades por estado:");
		for (String estado : mapa.keySet()) {
			System.out.println("\tEstado:" + estado);
			for (String cidade : mapa.get(estado)) {
				System.out.println("\t\tCidade:" + cidade);
			}
		}
	}

	private static void exemploMapaSimples() {
		// � interessante documentar, se necess�rio, o significado da chave e do
		// valor no mapa.
		// Mapa <nome-da-disciplina, nome-do-professor>
		Map<String, String> mapa = new HashMap<>();
		// Para colocar itens no mapa, devo indicar a chave e o correspondente
		// valor.
		mapa.put("POO", "Claudio");
		mapa.put("Compiladores", "Osvaldo");
		mapa.put("Web", "Mario");
		// Nos mapas, s� existe uma inst�ncia de valor associada a cada valor de
		// chave, e consequentemente a chave n�o pode ser duplicada. Se um novo
		// valor for associado a uma chave j� existente, este valor ir� sobrepor
		// o valor atualemente armazenado no mapa.
		// Neste exemplo, POO deixa de estar associada a Claudio e passa a ser
		// associada � Clara.
		mapa.put("POO", "Clara");

		// Listar as chaves de uma mapa (elas s�o um Set).
		System.out.println("Chaves do mapa: " + mapa.keySet());

		// Listar os valores do mapa
		System.out.println("Valores do mapa:" + mapa.entrySet());

		// � poss�vel obter o valor associado � chave
		System.out.println("Valor para a chave Compiladores: " + mapa.get("Compiladores"));

		// � poss�vel varrer o mapa e tratar cada valor individualmente
		System.out.println("Valores do mapa, tratados individualmente:");
		for (String chave : mapa.keySet()) {
			System.out.println("\t" + chave + " x " + mapa.get(chave));
		}
	}

}
