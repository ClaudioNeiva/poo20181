package br.ucsal.bes.poo20181.aula05.tui;

import java.util.Date;

import br.ucsal.bes.poo20181.aula05.domain.Aluno;

public class AlunoTui {

	public static void main(String[] args) {

		Aluno aluno1 = new Aluno("Claudio Neiva", "antonioclaudioneiva@gmail.com", new Date());
		Aluno aluno2 = new Aluno("Maria");
		Aluno aluno3 = new Aluno("Pedro");
		Aluno aluno4 = new Aluno();

		aluno4.setNome("Joaquim");

		System.out.println("aluno1.matricula=" + aluno1.getMatricula());
		System.out.println("aluno1.nome=" + aluno1.getNome());
		System.out.println("aluno2.matricula=" + aluno2.getMatricula());
		System.out.println("aluno2.nome=" + aluno2.getNome());
		System.out.println("aluno3.matricula=" + aluno3.getMatricula());
		System.out.println("aluno3.nome=" + aluno3.getNome());
		System.out.println("aluno4.matricula=" + aluno4.getMatricula());
		System.out.println("aluno4.nome=" + aluno4.getNome());

	}

}
