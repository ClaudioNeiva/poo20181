package br.ucsal.bes.poo20181.aula05.domain;

import java.util.Date;

public class Aluno {

    // Atributo de classe
	private static Integer contador = 0;

	private Integer matricula;

	private String nome;

	private String email;

	private Date dataNascimento;

	public Aluno() {
		// gerar um número de matrícula para cada novo aluno instanciado
		contador++;
		matricula = contador;
	}

	public Aluno(String nome) {
		this();
		this.nome = nome;
	}

	public Aluno(String nome, String email, Date dataNascimento) {
		this(nome);
		this.email = email;
		this.dataNascimento = dataNascimento;
	}

	public Integer getMatricula() {
		return matricula;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

}
