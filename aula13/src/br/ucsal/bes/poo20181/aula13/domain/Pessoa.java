package br.ucsal.bes.poo20181.aula13.domain;

import java.util.List;

public abstract class Pessoa {

	protected String nome;

	private List<String> telefones;

	private String logradouro;

	private String numero;

	private String bairro;

	private String cep;

	public Pessoa() {
		// System.out.println("constru� uma pessoa...");
	}

	public Pessoa(String nome) {
		super();
		this.nome = nome;
	}

	public Pessoa(String nome, String logradouro, List<String> telefones, String numero, String bairro, String cep) {
		super();
		this.nome = nome;
		this.telefones = telefones;
		this.logradouro = logradouro;
		this.numero = numero;
		this.bairro = bairro;
		this.cep = cep;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<String> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<String> telefones) {
		this.telefones = telefones;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	@Override
	public String toString() {
		return "Pessoa [nome=" + nome + ", telefones=" + telefones + ", logradouro=" + logradouro + ", numero=" + numero
				+ ", bairro=" + bairro + ", cep=" + cep + "]";
	}

	public abstract String apresentar();

}
