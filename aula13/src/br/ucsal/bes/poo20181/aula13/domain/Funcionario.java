package br.ucsal.bes.poo20181.aula13.domain;

import java.util.Date;

public class Funcionario extends PessoaFisica {

	String ctps;

	Date dataAdmissao;

	public Funcionario(String nome, String cpf, String nomeMae, Date dataNascimento, String ctps, Date dataAdmissao) {
		super(nome, cpf, nomeMae, dataNascimento);
		this.ctps = ctps;
		this.dataAdmissao = dataAdmissao;
	}

	public String getCtps() {
		return ctps;
	}

	public void setCtps(String ctps) {
		this.ctps = ctps;
	}

	public Date getDataAdmissao() {
		return dataAdmissao;
	}

	public void setDataAdmissao(Date dataAdmissao) {
		this.dataAdmissao = dataAdmissao;
	}

	@Override
	public String apresentar() {
		return "CTPS=" + ctps + " | nome=" + nome;
	}

	@Override
	public String toString() {
		return "Funcionario [ctps=" + ctps + ", dataAdmissao=" + dataAdmissao + "]";
	}

}
