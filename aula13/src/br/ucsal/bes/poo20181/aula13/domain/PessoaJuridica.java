package br.ucsal.bes.poo20181.aula13.domain;

import java.util.Date;

public class PessoaJuridica extends Pessoa {

	private String cnpj;

	private String inscricaoEstadual;

	private String inscricaoMunicipal;

	private Date dataAbertura;

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getInscricaoEstadual() {
		return inscricaoEstadual;
	}

	public void setInscricaoEstadual(String inscricaoEstadual) {
		this.inscricaoEstadual = inscricaoEstadual;
	}

	public String getInscricaoMunicipal() {
		return inscricaoMunicipal;
	}

	public void setInscricaoMunicipal(String inscricaoMunicipal) {
		this.inscricaoMunicipal = inscricaoMunicipal;
	}

	public Date getDataAbertura() {
		return dataAbertura;
	}

	public void setDataAbertura(Date dataAbertura) {
		this.dataAbertura = dataAbertura;
	}

	@Override
	public String apresentar() {
		return "CNPJ=" + cnpj + " | nome=" + nome;
	}

	@Override
	public String toString() {
		return "PessoaJuridica [cnpj=" + cnpj + ", inscricaoEstadual=" + inscricaoEstadual + ", inscricaoMunicipal="
				+ inscricaoMunicipal + ", dataAbertura=" + dataAbertura + "]";
	}

}
