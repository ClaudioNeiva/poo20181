package br.ucsal.bes.poo20181.aula13.domain;

import java.util.Date;

public class PessoaFisica extends Pessoa {

	private String cpf;

	private String nomeMae;

	private Date dataNascimento;

	public PessoaFisica() {
		super();
		// System.out.println("constru� uma pessoa f�sica...");
	}

	public PessoaFisica(String nome, String cpf, String nomeMae, Date dataNascimento) {
		super(nome);
		this.cpf = cpf;
		this.nomeMae = nomeMae;
		this.dataNascimento = dataNascimento;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNomeMae() {
		return nomeMae;
	}

	public void setNomeMae(String nomeMae) {
		this.nomeMae = nomeMae;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	@Override
	public String apresentar() {
		return "CPF=" + cpf + " | nome=" + nome;
	}

	@Override
	public String toString() {
		return super.toString() + "| PessoaFisica [cpf=" + cpf + ", nomeMae=" + nomeMae + ", dataNascimento="
				+ dataNascimento + "]";
	}

}
