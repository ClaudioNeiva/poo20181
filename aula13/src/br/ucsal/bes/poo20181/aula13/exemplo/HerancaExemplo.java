package br.ucsal.bes.poo20181.aula13.exemplo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.ucsal.bes.poo20181.aula13.domain.Funcionario;
import br.ucsal.bes.poo20181.aula13.domain.Pessoa;
import br.ucsal.bes.poo20181.aula13.domain.PessoaFisica;
import br.ucsal.bes.poo20181.aula13.domain.PessoaJuridica;

public class HerancaExemplo {

	public static void main(String[] args) {
		List<Pessoa> pessoas = new ArrayList<>();
		// List<PessoaFisica> pessoasFisicas = new ArrayList<>();
		// List<Funcionario> funcionarios = new ArrayList<>();

		PessoaFisica pessoaFisica1 = new PessoaFisica("claudio", "123", "lais", new Date());
		pessoas.add(pessoaFisica1);
		// pessoasFisicas.add(pessoaFisica1);
		System.out.println("pessoaFisica1=" + pessoaFisica1);

		Funcionario funcionario1 = new Funcionario("pedro", "345", "maria", new Date(), "7677", new Date());
		// funcionarios.add(funcionario1);
		pessoas.add(funcionario1);
		System.out.println("funcionario1=" + funcionario1);

		PessoaJuridica pessoaJuridica1 = new PessoaJuridica();
		pessoaJuridica1.setNome("Loja do Zezinho");
		pessoas.add(pessoaJuridica1);

		System.out.println("***************************************");

		exibir(pessoaFisica1);
		exibir(funcionario1);

		System.out.println("***************************************");

		exibirNomes(pessoas);

		System.out.println("***************************************");

		// Classes abstratas n�o podem ser instanciadas.
		// Pessoa pessoa1 = new Pessoa();
		// Se fosse poss�vel instanciar Pessoa, qual seria o comportamento ao
		// chamar o m�todo apresentar()????
		// pessoa1.apresentar();

		apresentar(pessoas);

	}

	private static void apresentar(List<Pessoa> pessoas) {
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa.apresentar());
		}
	}

	// Arquitetura RUIM, n�o fa�a assim!
	private static void exibirNomes(List<Pessoa> pessoas) {
		String apresentacao = "";
		for (Pessoa pessoa : pessoas) {

			// Isso � poss�vel, mas geralmente visto como uma gambiarra! N�o
			// fa�a, a n�o ser que n�o tenha outra alternativa.
			if (pessoa instanceof Funcionario) {
				apresentacao = "CTPS=" + ((Funcionario) pessoa).getCtps();
			} else if (pessoa instanceof PessoaFisica) {
				apresentacao = "CPF=" + ((PessoaFisica) pessoa).getCpf();
			} else if (pessoa instanceof PessoaJuridica) {
				apresentacao = "CNPJ=" + ((PessoaJuridica) pessoa).getCnpj();
			}

			apresentacao += "| nome=" + pessoa.getNome();

			System.out.println(apresentacao);

		}
	}

	private static void exibir(Pessoa pessoa) {
		System.out.println("pessoa=" + pessoa);
	}

}
