package br.ucsal.bes.poo20181.aula12.exemplo;

public enum EstadoCivilEnum {

	CASADO, SOLTEIRO, VIUVO, SEPARADO, PENSANDO_EM_SEPARAR, OUTROS;

}
