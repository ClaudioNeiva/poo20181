package br.ucsal.bes.poo20181.aula12.exemplo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class ExemploConversaoDate {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		converterStringDate();
	}

	private static void converterStringDate() {
		System.out.println("Informe a data de nascimento (dd/mm/aaaa):");
		String dataNascimentoString = scanner.nextLine();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Date dataNascimento = sdf.parse(dataNascimentoString);
			System.out.println("Data de nascimento=" + sdf.format(dataNascimento));
		} catch (ParseException e) {
			System.out.println("Data inv�lida!");
		}
	}

}
