package br.ucsal.bes.poo20181.aula12.exemplo;

import java.util.Scanner;

public class ExemploConversaoEnum {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		// converterStringEnumeracao1();
		// converterStringEnumeracao2();
		converterStringEnumeracao3();
	}

	// COMO N�O FAZER!!!
	public static void converterStringEnumeracao1() {
		System.out.println("Informe um estado civil (CASADO, SOLTEIRO, VIUVO, OUTROS):");
		String estadoCivilString = scanner.nextLine();
		EstadoCivilEnum estadoCivilEnum;

		if (estadoCivilString.equalsIgnoreCase("CASADO")) {
			estadoCivilEnum = EstadoCivilEnum.CASADO;
		} else if (estadoCivilString.equalsIgnoreCase("SOLTEIRO")) {
			estadoCivilEnum = EstadoCivilEnum.SOLTEIRO;
		} else if (estadoCivilString.equalsIgnoreCase("VIUVO")) {
			estadoCivilEnum = EstadoCivilEnum.VIUVO;
		} else if (estadoCivilString.equalsIgnoreCase("OUTROS")) {
			estadoCivilEnum = EstadoCivilEnum.OUTROS;
		} else {
			System.out.println("Estado civil inv�lido!");
			return;
		}

		System.out.println("Estado civil = " + estadoCivilEnum);
	}

	// UMA IMPLEMENTA��O MAIS OU MENOS LEGAL
	public static void converterStringEnumeracao2() {
		System.out.println("Informe um estado civil (CASADO, SOLTEIRO, VIUVO, OUTROS):");
		String estadoCivilString = scanner.nextLine();
		EstadoCivilEnum estadoCivilEnum = EstadoCivilEnum.valueOf(estadoCivilString.toUpperCase());
		System.out.println("Estado civil = " + estadoCivilEnum);
	}

	// UMA IMPLEMENTA��O LEGAL
	private static void converterStringEnumeracao3() {
		System.out.println("Informe um estado civil (" + listarItensEstadoCivilEnum() + "):");
		String estadoCivilString = scanner.nextLine();
		EstadoCivilEnum estadoCivilEnum = EstadoCivilEnum.valueOf(estadoCivilString.toUpperCase());
		System.out.println("Estado civil = " + estadoCivilEnum);
	}

	private static String listarItensEstadoCivilEnum() {
		String itens = "";
		for (EstadoCivilEnum item : EstadoCivilEnum.values()) {
			// itens = itens + item + ",";
			itens += item + ",";
		}
		return itens.substring(0, itens.length() - 1);
	}

	public static void manipularVetores() {
		for (EstadoCivilEnum item : EstadoCivilEnum.values()) {
			System.out.println(item);
		}

		for (int i = 0; i < EstadoCivilEnum.values().length; i++) {
			EstadoCivilEnum item = EstadoCivilEnum.values()[i];
			System.out.println(item);
		}

		EstadoCivilEnum[] itens = EstadoCivilEnum.values();
		for (int i = 0; i < itens.length; i++) {
			EstadoCivilEnum item = itens[i];
			System.out.println(item);
		}
	}
}
