package br.ucsal.bes.poo20181.aula12.heranca.sem;

import java.util.Date;
import java.util.List;

public class Pessoa {
	
	String cpf;

	String nome;

	List<String> telefones;

	Date dataNascimento;

	String nomeMae;

	String logradouro;

	String numero;

	String bairro;

	String cnpj;

	String inscricaoEstadual;

	String inscricaoMunicipal;

}
