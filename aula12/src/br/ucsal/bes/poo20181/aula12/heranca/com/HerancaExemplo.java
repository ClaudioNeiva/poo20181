package br.ucsal.bes.poo20181.aula12.heranca.com;

public class HerancaExemplo {

	public static void main(String[] args) {

		PessoaFisica pessoaFisica1 = new PessoaFisica();

		pessoaFisica1.nome = "Claudio";
		pessoaFisica1.cep = "40000000";

		Funcionario funcionario1 = new Funcionario();

		// Upcast -> "convers�o" para uma classe superior.
		PessoaFisica funcionario2 = new Funcionario();

		funcionario1.ctps = "123123";

		// Como funcion�rio 2 � da classe PessoaFisica, n�o ser� poss�vel
		// manipular os atributos espec�ficos de Funcionario.
		// funcionario2.ctps = "23842";

		// Downcast -> "convers�o" para uma subclasse.
		Funcionario funcionario3 = (Funcionario) funcionario2;
		funcionario3.ctps = "76767";

		PessoaFisica pessoaFisica2 = new PessoaFisica();

		// Nem todo downcast � poss�vel. Neste caso, ocorrer� um erro de
		// execu��o.
		Funcionario funcionario4 = (Funcionario) pessoaFisica2;
		funcionario4.ctps = "985498";
	}

}
