package br.ucsal.bes.poo20181.aula10;

public class Veiculo implements Comparable<Veiculo>{

	private String placa;

	private Integer anoFabricacao;

	private TipoVeiculoEnum tipo;

	public Veiculo(String placa, Integer anoFabricacao, TipoVeiculoEnum tipo) {
		this.placa = placa;
		this.anoFabricacao = anoFabricacao;
		this.tipo = tipo;
	}

	public String getPlaca() {
		return placa;
	}

	public Integer getAnoFabricacao() {
		return anoFabricacao;
	}

	public TipoVeiculoEnum getTipo() {
		return tipo;
	}

	public void setTipo(TipoVeiculoEnum tipo) {
		this.tipo = tipo;
	}

	@Override
	public String toString() {
		return "Veiculo [placa=" + placa + ", anoFabricacao=" + anoFabricacao + ", tipo=" + tipo + "]";
	}

	// Implementa��o inicial, n�o � assim que deve ser feito, mas a princ�pio
	// vai ajudar no racioc�nio;
	@Override
	public int compareTo(Veiculo outroVeiculo) {
		if(anoFabricacao > outroVeiculo.anoFabricacao){
			return 1;
		}
		if(anoFabricacao < outroVeiculo.anoFabricacao){
			return -1;
		}
		return 0;
	}
}
