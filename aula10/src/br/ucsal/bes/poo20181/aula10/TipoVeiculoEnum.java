package br.ucsal.bes.poo20181.aula10;

public enum TipoVeiculoEnum {

	BASICO(100.45), INTERMEDIARIO(130.10), LUXO(156.00);
	
	private Double valor;

	private TipoVeiculoEnum(Double valor){
		this.valor = valor;
	}
	
	public Double getValor(){
		return valor;
	}

}
