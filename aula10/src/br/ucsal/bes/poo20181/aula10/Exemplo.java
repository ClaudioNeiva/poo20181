package br.ucsal.bes.poo20181.aula10;

import java.util.Scanner;

public class Exemplo {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

		Veiculo veiculo1 = new Veiculo("ABC-1234", 2017, TipoVeiculoEnum.BASICO);
		Veiculo veiculo2 = new Veiculo("XYZ-8765", 2015, TipoVeiculoEnum.LUXO);
		Veiculo veiculo3 = new Veiculo("PET-1122", 2018, TipoVeiculoEnum.INTERMEDIARIO);
		Veiculo veiculo4 = new Veiculo("", 2018, TipoVeiculoEnum.BASICO);

		veiculo1.setTipo(TipoVeiculoEnum.LUXO);

		Contrato contrato1 = new Contrato("Claudio", "Rua x");
		contrato1.adicionarVeiculo(veiculo1);

		System.out.println("veiculo1 -> Placa=" + veiculo1.getPlaca());
		System.out.println("veiculo2 -> Placa=" + veiculo2.getPlaca());

	}

}
