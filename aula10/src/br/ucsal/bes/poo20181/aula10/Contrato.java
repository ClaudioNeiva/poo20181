package br.ucsal.bes.poo20181.aula10;

import java.util.ArrayList;
import java.util.List;

public class Contrato {
	
	private static Integer seq = 0;

	private Integer numero;

	private String nomeCliente;

	private String enderecoCliente;

	private List<Veiculo> veiculos = new ArrayList<>();

	private Double valorTotal = 0d;

	public Contrato(String nomeCliente, String enderecoCliente) {
		this.nomeCliente = nomeCliente;
		this.enderecoCliente = enderecoCliente;
		seq++;
		this.numero = seq;
	}

	/*
	 * Boolean adicionarVeiculo(Veiculo veiculo):
	 * 
	 * 1. Verificar se o ve�culo j� consta da lista de ve�culos a partir da
	 * placa do mesmo.
	 * 
	 * 1.1. Caso j� exista um ve�culo com a mesma placa: 1.1.1. N�o deve ser
	 * poss�vel adicionar o mesmo � lista 1.1.2. Retornar false.
	 * 
	 * 1.2. Caso o ve�culo n�o exista na lista: 1.2.1. Adicionar � lista 1.2.2.
	 * Adicionar o valor do mesmo (com base no tipo de ve�culo) ao valor total
	 * do contrato; 1.2.3. Retornar true.
	 */
	
	public Boolean adicionarVeiculo(Veiculo veiculo) {
		for(Veiculo veiculoAtual : veiculos){
			if (veiculoAtual.getPlaca().equals(veiculo.getPlaca())) {
				return false;
			}
		}
		veiculos.add(veiculo);
		valorTotal += veiculo.getTipo().getValor();
		return true;
	}

	public String getNomeCliente() {
		return nomeCliente;
	}

	public String getEnderecoCliente() {
		return enderecoCliente;
	}

	public Double getValorTotal() {
		return valorTotal;
	}
}
