package br.ucsal.bes.poo20181.aula10;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class ExemploLista {

	public static void main(String[] args) {

		// criarListaLiterais();

		criarListaVeiculos();

	}

	private static void criarListaVeiculos() {
		List<Veiculo> veiculos = new ArrayList<>();
		veiculos.add(new Veiculo("ABC-1234", 2010, TipoVeiculoEnum.LUXO));
		veiculos.add(new Veiculo("XYK-2934", 2012, TipoVeiculoEnum.INTERMEDIARIO));
		veiculos.add(new Veiculo("PTO-2783", 2010, TipoVeiculoEnum.LUXO));
		veiculos.add(new Veiculo("AAA-2341", 2012, TipoVeiculoEnum.LUXO));
		veiculos.add(new Veiculo("NTO-8923", 2011, TipoVeiculoEnum.BASICO));
		
		System.out.println("Os ve�culos, na ordem que foram adicionados � lista:");
		for(Veiculo veiculo : veiculos){
			System.out.println(veiculo.toString());
		}

		// Ordenar os ve�culos por ano de fabrica��o (crescente)
		Collections.sort(veiculos);
	
		System.out.println("Os ve�culos, na ordem crescente de ano de fabrica��o que foram adicionados � lista:");
		for(Veiculo veiculo : veiculos){
			System.out.println(veiculo.toString());
		}

	}

	private static void criarListaLiterais() {
		List<String> nomes = new ArrayList<>();
		nomes.add("claudio");
		nomes.add("maria");
		nomes.add("pedro");
		nomes.add("clara");
		nomes.add("roberto");

		System.out.println("Os nomes, na ordem que foram informados:");
		for (String nome : nomes) {
			System.out.println("nome = " + nome);
		}

		// Ordernar os itens de uma lista (crescente)
		Collections.sort(nomes);

		System.out.println("Os nomes, em ordem alfab�tica (crescente):");
		for (String nome : nomes) {
			System.out.println("nome = " + nome);
		}

		// Ordernar os itens de uma lista (decrescente)
		// O reverse N�O ORDENOU EM ORDEM DECRESCENTE, ELE APENAS INVERTEU A
		// ORDEM DOS ITENS DA LISTA.
		Collections.reverse(nomes);

		System.out.println("Os nomes, em ordem alfab�tica (decrescente):");
		for (String nome : nomes) {
			System.out.println("nome = " + nome);
		}

	}

}
